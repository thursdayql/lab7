
public class SecureCoin implements Lockable
{
  private int key = -1;

  private final int HEADS = 0;
  private final int TAILS = 1;

  private int face;

  //-----------------------------------------------------------------
  //  Sets up the coin by flipping it initially.
  //-----------------------------------------------------------------
  public SecureCoin()
  {
    flip();
  }

  //-----------------------------------------------------------------
  //  Flips the coin by randomly choosing a face value.
  //-----------------------------------------------------------------
  public void flip()
  {
    if (this.locked())
      face = (int) (Math.random() * 2);
    else
      System.out.println("Coin is Locked - NO Action");
  }

  public boolean setKey(int key)
  {
    return false;
  }

  public boolean lock(int key)
  {
    return false;
  }

  public boolean unlock(int key)
  {
    return false;
  }

  public boolean locked()
  {
    return false;
  }

}
