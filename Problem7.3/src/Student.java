//********************************************************************
//  Student.java       Author: Lewis/Loftus
//
//  Represents a college student.
//********************************************************************

public class Student
{
  private String firstName, lastName;
  private Address homeAddress, schoolAddress;
  private int score1 = -1, score2 = -1, score3 = -1;

  //-----------------------------------------------------------------
  //  Constructor: Sets up this student with the specified values.
  //-----------------------------------------------------------------
  public Student(String first, String last, Address home, Address school,
      int score1, int score2, int score3)
  {
    firstName = first;
    lastName = last;
    homeAddress = home;
    schoolAddress = school;
    this.score1 = score1;
    this.score2 = score2;
    this.score3 = score3;
  }

  public Student(String first, String last, Address home, Address school)
  {
    firstName = first;
    lastName = last;
    homeAddress = home;
    schoolAddress = school;
    this.score1 = 0;
    this.score2 = 0;
    this.score3 = 0;
  }

  public void setTestScore(int testNumber, int score)
  {
    switch (testNumber)
    {
    case 1:
      this.score1 = score;
      break;
    case 2:
      this.score2 = score;
      break;
    case 3:
      this.score3 = score;
      break;
    default:
      break;
    }
  }

  public int getTestScore(int testNumber)
  {

    int score = -1;

    switch (testNumber)
    {
    case 1:
      score = this.score1;
      break;
    case 2:
      score = this.score2;
      break;
    case 3:
      score = this.score3;
      break;
    default:
      break;
    }
    return score;
  }

  public double average()
  {
    return (score1 + score2 + score3) / 3.0;
  }

  //-----------------------------------------------------------------
  //  Returns a string description of this Student object.
  //-----------------------------------------------------------------
  public String toString()
  {
    String result;

    result = firstName + " " + lastName + "\n";
    result += "Home Address:\n" + homeAddress + "\n";
    result += "School Address:\n" + schoolAddress;
    result += "\n Score 1: " + score1;
    result += "\n Score 2: " + score2;
    result += "\n Score 3: " + score3;
    result += "\n Average: " + this.average();

    return result;
  }
}
